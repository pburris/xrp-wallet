#!/bin/bash

# Kill all containers
docker kill $(docker ps -q)

# Start Database
docker run --name postgresql -e POSTGRES_PASSWORD=ripple -e POSTGRES_USER=ripple -e POSTGRES_DB=ripple -p 5432:5432 -d postgres
