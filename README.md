# Ripple (XRP) Wallet Client

This is an XRP wallet. XRP is the native cryptocurrency on the [Ripple network](https://ripple.com/). Ripple is a blockchain company that aims to make currency to currency transactions faster, cheaper, and more efficient, especially for currencies with inefficient conversion channels.

As Ripple puts it:

>Ripple works with banks to transform how they send money around the world... Using Ripple, banks can meet growing demands for faster, low-cost, on-demand global payment services for any payment size. The result: new revenue opportunities, lower processing costs, and better overall customer experiences.`

## Roadmap

  1. Express backend REST API for creating and using a Ripple wallet
  2. React frontend for the browser, with offline-first capabilities
  3. Add Google 2FA
  4. React Native application sharing the same backend as the web, offline first as well
  5. Move current application onto a DAPP network (lisk?)
  6. Electron application for native Windows/OSX/Linux experience


## Idea

Use a DAPP to create an XRP wallet that can be accessed from as many devices as possible without any cumbersome process of moving the wallet from device to device. I want to be able to own my wallet's key and secret while being able to securely log into the wallet from anywhere at anytime using 2fa
