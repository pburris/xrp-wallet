import Sequelize from 'sequelize';
import config from './config';

const { db } = config;
const { database, user, password, host } = db;
/**
 * Create and export database connection
 */
const sequelize = new Sequelize(database, user, password, {
  host: host,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});

export default sequelize;
