import ripple, { rippleApi as api } from '../lib/ripple';


/**
 * Payment
 *
 * @description Payment transaction
 */
export const txPayment = (tx, router) => {
  const payment = {
    source: {
      address: tx.sourceAddress,
      maxAmount: {
        value: tx.sendValue,
        currency: 'XRP',
      },
    },
    destination: {
      address: tx.destAddress,
      amount: {
        value: tx.sendValue,
        currency: 'XRP',
      },
    },
  };

  api.connect()
    .then(() => {
      // Prepare Transaction
      /**
       * Transaction Type: Payment
       * Preparing a payment transaction takes 3 params
       * @param {String} address, address to pay out to
       * @param {Object} payment, payment object with information about the payment tx (to and from)
       * @param {Object} instructions, Optional Instructions for executing the transaction
       */
      const address = tx.sourceAddress;
      const paymentInstructions = {};

      return api.preparePayment(address, payment, paymentInstructions)
        .then(({ txJSON, instructions}) => {
          // Sign Transaction
          /**
           * Sign the prepared transaction
           * @param {String} txJson, Transaction represented as a JSON string in rippled format
           * @param {String} secret, The secret of the account that is initiating the transaction
           * @param {Object} options, Optional Options that control the type of signature that will be generated
           */
          const secret = tx.sourceSecret;
          const signOptions = {};
          const signedTx = api.sign(txJSON, secret, signOptions);
          return signedTx;
        })
        .then(signedTx => {
          // Submit Transaction
          const { signedTransaction, id } = signedTx;
          const submittedTransaction = api.submit(signedTransaction);
          const resWithTxId = { submittedTransaction, id };
          return resWithTxId;
        })
        .then(({ submittedTransaction, id}) => {
          // Verify Transaction
          // Result Logic
          submittedTransaction
            .then(txRes => {
              router.send({
                code: txRes.resultCode,
                message: txRes.resultMessage,
              });
            })
            .then(() => api.disconnect())
            .catch(console.error);
        })
        .catch(console.error);
    })
    .catch(console.error);
}
