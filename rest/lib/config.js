/**
 * Config
 */
const env = process.env;

export default {
  server: {
    port: env.PORT || 3001,
		secret: env.SECRET || 'This is my secret',
  },
  db: {
    user: env.DB_USER || 'ripple',
    password: env.DB_PASSWORD || 'ripple',
    database: env.DB_NAME || 'ripple',
    host: env.DB_HOST || 'localhost',
  },
};
