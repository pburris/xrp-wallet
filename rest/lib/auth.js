/**
 * Authentication via Passport
 */
import passport from 'passport';
import bCrypt from 'bcrypt-nodejs';
import jwt from 'jwt-simple';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { User } from '../models';
import config from './config';

// Serialize User
passport.serializeUser((user, done) => done(null, user.id));

// Deserialize User
passport.deserializeUser((id, done) =>
  User.findOne({ where: { id } })
    .then(user =>
      user ? done(null, user.get()) : done(user.errors, null))
    .catch(err => done(err, null))
);

/**
 * Generate Password Hash
 *
 * @description Create a hashed password from a plain text password
 * @param {String} password to be hashed
 * @return {String} hashed password
 */
const generateHash = password => bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);

/**
 * Generate JWT
 *
 * @description Generate a JSON Web Token (JWT) for user
 * @param {Object} user object to be turned into a token
 * @return {String} JWT
 */
const generateJwt = user =>
  jwt.encode({ sub: user.id, iat: new Date().getTime() }, config.server.secret)

/**
 * Is Valid Password
 *
 * @description Figure out if the supplied password is valid
 * @param {String} userPass of the user to check the password against
 * @param {String} hashedPass password from the db
 * @return {Boolean} is the password valid?
 */
const isValidPassword = (userPass, hashedPass) => bCrypt.compareSync(hashedPass, userPass);

/**
 * isLoggedIn
 *
 * @description Middleware to check and see if user is logged in
 */
const isLoggedIn = (req, res, next) =>
  req.isAuthenticated() ?
    next() :
    res.redirect('/');

/**
 * filterUser
 *
 * @description Filter user credentials
 * @param {Object} user, user object
 * @return {Object} user, filtered user object
 */
const filterUser = user => {
  const { id, firstName, lastName, email, createdAt, wallets } = user;
  const filteredUser = {
    id,
    firstName,
    lastName,
    email,
    createdAt,
    wallets,
  };
  return user.getWallet()
    .then(wallets => {
      const filteredWallets = wallets.map(
        ({ id, address, createdAt }) => ({ id, address, createdAt }));
      filteredUser.wallets = filteredWallets;
      return filteredUser;
    })
}

/**
 * Local Signin Strategy
 */
const localStratOpts = {
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true,
};

const localStratSigninVerify = (req, email, password, done) =>
  User.findOne({ where: { email } })
    .then(user => {
      if (!user) return done(null, false, { message: 'Email does not exist' });
      if (!isValidPassword(user.password, password))
        return done(null, false, { message: 'Incorrect password.' });
      const userInfo = user.get();
      req.login(user, err => err ? done(err, false) : done(null, userInfo));
    })
    .catch(err => done(null, false, { message: 'Something went wrong with your Signin' }));


/**
 * JWT Strategy
 */
const jwtStratOpts = {
  secretOrKey: config.server.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeader('auth'),
};

const jwtStratVerify = (payload, done) => {
  User.findOne({ where: { id: payload.sub } })
    .then(user => user ? done(null, user) : done(null, false))
    .catch(err => done(err, false))
}

/**
 * Passport.use()
 */
passport.use('local-signin', new LocalStrategy(localStratOpts, localStratSigninVerify));
passport.use(new JwtStrategy(jwtStratOpts, jwtStratVerify));


export { generateHash, generateJwt, isValidPassword, isLoggedIn, filterUser };
export default passport;
