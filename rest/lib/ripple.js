import { RippleAPI } from 'ripple-lib';


const rippleApi = new RippleAPI({
  server: 'wss://s1.ripple.com', //  Public rippled server hosted by Ripple
});


// On Error
rippleApi.on('error', (errCode, errMsg) => console.log(`${errCode}: ${errMsg}`));

// Connected
rippleApi.on('connected', () => console.log('Connected to Ripple server'));

// Disconnect
rippleApi.on('disconnected', code => console.log(`Disconected, code: ${code}`));


/**
 * API Wrapper
 *
 * @description This wraps the api connection call and allows me to pass in my callbacks
 */
const ripple = cb =>
  rippleApi.connect()
    .then(() => {
      if (rippleApi.isConnected()) {
        return cb();
      }
      console.error('Not Connected!');
    })
    .then(() => rippleApi.disconnect())
    .catch(console.error);

export { rippleApi };
export default ripple;
