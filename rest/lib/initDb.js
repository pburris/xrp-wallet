import db from './db';
import { User, Wallet } from '../models';
import { generateHash } from './auth';

/**
 * Initialize Database
 *
 * @description setup the database and make sure that we create a test user with a dummy wallet
 */
export default function initDb() {

db.sync()
  .then(() =>
    db.authenticate()
      .then(() => console.log('\n Database Connection Established! \n'))
      .then(() => {
        // Create Dummy User and Wallet
        const dummyUserData = {
          email: 'test@test.com',
          password: generateHash('p'),
          firstName: 'first',
          lastName: 'last',
        };
        User.create(dummyUserData)
          .then((user, created) => {
            const dummyWalletData = {
              address: 'rEVSxAXSShG3MsuhjGNgeu7reJCLf4txz9',
              secret: 'shZUgWh4Gt47Qmrw91KunjSn76RVi',
              userId: 1,
            };
            Wallet.create(dummyWalletData)
              .then((wallet, created) => {
                console.log(wallet);
              })
          })
      })
      .catch(err => console.log(err)))
  .catch(err => console.log(err));

}
