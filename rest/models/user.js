/**
 * User Model
 */
import Sequelize from 'sequelize';
import db from '../lib/db';
import config from '../lib/config';
import { generateHash } from '../lib/auth';


const User = db.define('user', {
  firstName: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});


export default User;
