import db from '../lib/db';
import User from './user';
import Wallet from './wallet';


// Associations
User.hasMany(Wallet, { as: 'Wallet' });
Wallet.belongsTo(User, { as: 'User', foreignKey: 'userId' });


export {
  User,
  Wallet,
};
