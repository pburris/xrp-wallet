/**
 * User Model
 */
import Sequelize from 'sequelize';
import db from '../lib/db';
import config from '../lib/config';
import { generateHash } from '../lib/auth';


const Wallet = db.define('wallet', {
  address: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  secret: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});


export default Wallet;
