/**
 * Routing helpers
 */
import passport, { generateJwt } from '../lib/auth';

const helpers = {};

// Local Signin Middleware
helpers.signinLocal = passport.authenticate('local-signin', { session: false });

// Require JWT authorization
helpers.requireAuth = passport.authenticate('jwt', { session: false });

export default helpers;
