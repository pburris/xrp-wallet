/**
 * User Routes
 */
import express from 'express';
import controller from '../controllers/user';
const router = express.Router();

/* GET users */
router.get('/', controller.all);

/* GET user by ID */
router.get('/:id', controller.single);

/* PUT user by ID */
router.put('/:id', controller.edit);

/* DELETE user by ID */
router.delete('/:id', controller.del);

export default router;
