/**
 * Pages Routes
 */
import express from 'express';
import helpers from './helpers';
const router = express.Router();

router.get('/', (req, res) => {
  res.send({
    message: 'API root',
  });
});

router.get('/secret', helpers.requireAuth, (req, res) => {
  res.send({
    message: 'You made it through',
  });
})

export default router;
