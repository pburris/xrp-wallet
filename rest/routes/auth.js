/**
 * Auth Routes
 */
import express from 'express';
import passport from '../lib/auth';
import controller from '../controllers/auth';
import helpers from './helpers';

const router = express.Router();

/* GET check if user is logged in, sends auth information back */
router.get('/me', helpers.requireAuth, controller.me);

/* POST sign up a new user */
router.post('/signup', controller.signup);

/* POST sign in */
router.post('/signin', helpers.signinLocal, controller.signin);

/* GET log out */
router.get('/signout', controller.logout);


export default router;
