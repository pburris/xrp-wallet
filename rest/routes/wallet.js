import express from 'express';
import { getBalance, createNewWallet, transaction } from '../controllers/wallet';
import helpers from './helpers';

const router = express.Router();


router.get('/', (req, res) => res.send({ message: 'Root url'}));

router.get('/balance/:wallet', getBalance);

router.get('/new', helpers.requireAuth, createNewWallet);

router.post('/transaction', helpers.requireAuth, transaction);

export default router;
