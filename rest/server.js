import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import session from 'express-session';
import cookieSession from 'cookie-session';
import cors from 'cors';

import config from './lib/config';
import passport from './lib/auth';
import initDb from './lib/initDb';

import walletRoutes from './routes/wallet';
import pagesRoutes from './routes/pages';
import authRoutes from './routes/auth';
import userRoutes from './routes/user';


// Create Express app
const app = express();


// Initialize DB
initDb();


/**
 * Middleware
 **/
app.use(morgan('combined'));
app.use(cookieSession({
  name: 'session',
  keys: [config.server.secret],
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    secret: config.server.secret,
    resave: true,
    saveUninitialized: true,
  }
));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());


/**
 * Routes
 */
app.use('/', pagesRoutes);
app.use('/wallet', walletRoutes);
app.use('/auth', authRoutes);
app.use('/users', userRoutes);


app.listen(config.server.port,
  () => console.log(`Server running on port: ${config.server.port} \n`));
