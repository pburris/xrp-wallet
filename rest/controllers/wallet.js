import ripple, { rippleApi } from '../lib/ripple';
import { Wallet, User } from '../models';
import { txPayment } from '../lib/transactions';


/**
 * Get balance from a wallet
 *
 * @description Get the current balance of a given wallet
 */
export const getBalance = (req, res) =>
  ripple(
    () => rippleApi.getBalances(req.params.wallet)
      .then(balances => res.send(balances))
      .catch(console.error)
  );

/**
 * Create New Wallet
 *
 * @description Create a new wallet, save it to the user and return the information
 */
export const createNewWallet = (req, res) =>
  ripple(
    () => {
      const newWallet = rippleApi.generateAddress();
      newWallet.userId = res.req.user.id;
      Wallet.create(newWallet)
        .then((wallet, created) => {
          res.send({
            id: wallet.id,
            address: wallet.address,
            createdAt: wallet.createdAt,
          })
        })
    }
  );

/**
 * Transaction
 *
 * @description Execute a transaction based on the information sent from the frontend
 */
export const transaction = (req, res) => {
  const { destAddress, sourceAddress, sendValue, type } = req.body;
  // Lookup up user and confirm the user owns the wallet first
  User.findOne({ email: res.req.user.email })
    .then(user => {
      user.getWallet()
        .then(wallets => {
          const verifyWallet = wallets.filter(wallet => wallet.address = sourceAddress);
          if (verifyWallet.length) {
            // Create transaction object
            const tx = {
              destAddress,
              sourceAddress,
              sourceSecret: verifyWallet[0].secret,
              sendValue,
              type,
            };

            // Figure out which transaction to execute
            switch (tx.type) {
              case 'payment':
                return txPayment(tx, res);
              default:
                return res.send({ message: 'Transaction Type Needed' });
            }
          }
          return res.send({ message: 'Cannot Verify Wallet'});
        })
        .catch(err => res.send({ message: err }))
    })
    .catch(err => res.send({ message: err }))

}
