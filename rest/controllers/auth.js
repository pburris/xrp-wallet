/**
 * Auth Controller
 *
 */
import { User } from '../models';
import { generateJwt, generateHash, filterUser } from '../lib/auth';

// Create authController
const authController = {};


/**
 * /me - JWT
 */
authController.me = (req, res) => filterUser(res.req.user).then(user => res.send(user));

/**
 * Sign Up - JWT
 */
authController.signup = (req, res, next) => {
  const { email, password, firstName, lastName } = req.body;
  console.log(req.body);
  // Check to see if password and email were sent
  if (!email || !password) {
    return res.status(422).send({ error: 'You must provide an email and password'});
  }
  // See if a user the the email exists
  User.findOne({ where: { email } })
    .then(existingUser => {
      // if user exists, send error
      if (existingUser) {
        return res.status(422).send({ error: 'Email already taken'});
      }
      // Hash password and create user object
      const hashPass = generateHash(password);
      const userData = {
        email,
        password: hashPass,
        firstName,
        lastName,
      };
      // Attempt to create user
      return User.create(userData)
        .then((newUser, created) => {
          filterUser(newUser)
            .then( filteredUser =>
              res.json({
                token: generateJwt(newUser.dataValues),
                user: filteredUser,
              })
            )
        })
        .catch(err => next(err));
    })
    .catch(err => next(err));
}


/**
 * Sign In - JWT
 */
authController.signin = (req, res, next) =>
  // User email and pass already auth'd
  User.findOne({ where: { email: req.user.email } })
    .then(user =>
      filterUser(user)
        .then(filteredUser =>
          res.send({ token: generateJwt(req.user), user: filteredUser })
        )
    )


/**
 * Logout - JWT
 */
authController.logout = (req, res, next) => {
  delete req.session.passport;
  res.send({ message: 'Logged out' });
}

export default authController;
