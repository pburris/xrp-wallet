/**
 * User Controller
 *
 */
import { User } from '../models';
import { generateHash } from '../lib/auth';

// Create userController
const userController = {};


/* All users */
userController.all = (req, res, next) =>
  User.findAll()
    .then(users => res.send(users.map((
      { id, firstName, lastName, email}) => ({ id, firstName, lastName, email})
    )))
    .catch(err => res.send(err));

/* Single User */
userController.single = (req, res, next) =>
  User.findOne({ where: { id: req.params.id } })
    .then(user => {
      if (!user) res.send({ error: 'User not found' });
      user.getWallet()
        .then(wallets => {
          const thisUser = {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            createdAt: user.createdAt,
            wallets: wallets.map(({ id, address, createdAt }) => ({ id, address, createdAt })),
          };
          res.send(thisUser);
        })
    })
    .catch(err => res.send(err));

/* Edit User */
userController.edit = (req, res, next) =>
  User.update(filterUserFields(req.body),
    { where : { id: req.params.id } })
  .then(() =>
    User.findOne({ where: { id: req.params.id } })
      .then(user => res.send(user))
      .catch(err => res.send(err)))
  .catch(err => res.send(err));

/* Delete User */
userController.del = (req, res, next) =>
  User.destroy({ where: { id: req.params.id } })
    .then(success => success ?
      res.send({ message: 'User successfully deleted'}) :
      res.send({ error: 'Problem deleting user'})
    )
    .catch(err => res.send(err));


/* Helpers */

// Filter User Fields
const filterUserFields = fields =>
  Object.assign({}, fields, {
    email: fields.email,
    password: generateHash(fields.password),
  });

export default userController;
