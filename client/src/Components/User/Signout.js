import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';


class Signout extends Component {
  componentWillMount() {
    this.props.signoutUser();
  }

  render() {
    return (
      <div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  signoutUser: () => dispatch(actions.signoutUser()),
});

export { Signout };
export default connect(null, mapDispatchToProps)(Signout);
