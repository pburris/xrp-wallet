import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from './actions';
import renderField from '../Forms/renderField';
import { withRouter } from 'react-router-dom';


const validate = values => {
  const errors = {};
  if (!values.email) errors.email = 'Email is required';
  if (!values.password) errors.password = 'Please enter password';
  return errors;
};


class UserSigninForm extends Component {
  handleFormSubmit(values, dispatch) {
    dispatch(actions.signinUser(values));
    this.props.history.push('/balance');
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={ handleSubmit(this.handleFormSubmit.bind(this)) }>

        <div className="form--section">
          <Field name="email" component={ renderField } type="email" label="Email" />
        </div>

        <div className="form--section">
          <Field name="password" component={ renderField } type="password" label="Password" />
        </div>

        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default withRouter(reduxForm({
  form: 'userSignin',
  validate,
}, null)(UserSigninForm));
