import createHistory from 'history/createBrowserHistory';
import { actions as errorActions } from '../Error';


const ROOT_URL = 'http://localhost:3001';
const getJwtHeader = token => ({ 'authorization': `JWT ${token}` });
const browserHistory = createHistory();

/**
 * Actions
 */
export const AUTH_USER = 'AUTH_USER';
export const UNAUTH_USER = 'UNAUTH_USER';
export const AUTH_ERROR = 'AUTH_ERROR';
export const SET_USER = 'SET_USER';
export const REMOVE_USER = 'REMOVE_USER';


/**
 * Action Creators
 */
export const authUser = () => ({
  type: AUTH_USER,
});

export const unauthUser = () => ({
  type: UNAUTH_USER,
});

export const authError = err => ({
  type: AUTH_ERROR,
  payload: err,
});

export const setUser = user => ({
  type: SET_USER,
  user,
});

export const removeUser = () => ({
  type: REMOVE_USER,
});


/**
 * Action Dispatcher Thunks
 */

/**
 * Sign in User
 */
export const signinUser = ({ email, password }) => {
  const opts = {
    method: 'POST',
    body: JSON.stringify({ email, password }),
    headers: {
      "Content-Type": "application/json",
    },
  };

  return dispatch =>
    fetch(`${ROOT_URL}/auth/signin`, opts)
      .then(res => res.json())
      .then((res) => {
        localStorage.setItem('token', res.token);
        dispatch(authUser());
        return dispatch(setUser(res.user));
      })
      .catch(err => dispatch(authError('Bad Login Info')));
}


/**
 * Sign out User
 */
export const signoutUser = () =>
  dispatch =>
    fetch(`${ROOT_URL}/auth/signout`)
      .then(res => res.json())
      .then((res) => {
        if (res.error) {
          console.log(res);
          localStorage.removeItem('token');
          dispatch(unauthUser());
          dispatch(removeUser());
          return dispatch(errorActions.addError(res.error));
        } else {
          localStorage.removeItem('token');
          dispatch(unauthUser());
          dispatch(removeUser());
        }
      })
      .catch(err => console.log(err))

/**
 * Sign Up User
 */
export const signupUser = (user) => {
  const opts = {
    method: 'POST',
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
  };

  return dispatch =>
    fetch(`${ROOT_URL}/auth/signup`, opts)
      .then(res => res.json())
      .then(res => {
        if (res.error) {
          return dispatch(errorActions.addError(res.error));
        }
        if (res.token) {
          localStorage.setItem('token', res.token);
          //browserHistory.push('/balance');
          dispatch(authUser());
          return dispatch(setUser(res.user));
        }
      })
      .catch(err => console.log(err))
}


/**
 * Using JWT
 */
export const getUser = () =>
  dispatch =>
    fetch(`${ROOT_URL}/auth/me`, { headers: getJwtHeader(localStorage.getItem('token')) })
      .then(res =>
        res.status === 401
          ? dispatch(errorActions.addError("Can't find user, Please login again"))
          : res.json()
      )
      .then(res => {
        console.log(res);
        return res;
      })
      .then(user => user.email ? dispatch(setUser(user)) : dispatch(user));
