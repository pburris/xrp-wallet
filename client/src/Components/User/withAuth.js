import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


const withAuth = condition =>
  BaseComponent => {
    class WithAuth extends Component {
      constructor(props) {
        super(props);
      }

      componentWillMount() {
        if (this.props.authenticated !== condition) {
          this.props.history.push('/');
        }
      }

      componentWillUpdate(nextProps) {
        if (nextProps.authenticated !== condition) {
          this.props.history.push('/');
        }
      }

      render() {
        return (
          <BaseComponent { ...this.props } />
        );
      }
    }

    const mapStateToProps = state => ({
      authenticated: state.auth.authenticated,
    });
    return withRouter(connect(mapStateToProps)(WithAuth))
  }

export default withAuth;
