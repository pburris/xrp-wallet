import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


const withUserInfo = BaseComponent => {
  const mapStateToProps = state => ({
    authenticated: state.auth.authenticated,
  });
  return connect(mapStateToProps)(BaseComponent);
}

export default withUserInfo;
