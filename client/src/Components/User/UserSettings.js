import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GenerateWallet } from '../Wallet';


class UserSettings extends Component {
  render() {
    return (
      <div>
        {
          this.props.user &&
          <h1>{`${this.props.user.firstName} ${this.props.user.lastName}`}</h1>
        }
        {
          this.props.user &&
          !this.props.user.wallet &&
          <GenerateWallet />
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(UserSettings);
