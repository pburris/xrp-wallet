import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form, Button } from 'antd';
import * as actions from './actions';
import renderField from '../Forms/renderField';
import { withRouter } from 'react-router-dom';


const validate = values => {
  const errors = {};
  if (!values.firstName) errors.firstName = 'First name is required';
  if (!values.lastName) errors.lastName = 'Last name is required';
  if (!values.email) errors.email = 'Email is required';
  if (!values.password) errors.password = 'Please enter password';
  if (!values.confirmPassword) errors.confirmPassword = 'Please confirm password';
  if (!(values.password === values.confirmPassword)) errors.password = 'Passwords must match';
  return errors;
};


class UserSignupForm extends Component {
  handleFormSubmit(values, dispatch) {
    dispatch(actions.signupUser(values));
    this.props.history.push('/balance');
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <Form
        onSubmit={ handleSubmit(this.handleFormSubmit.bind(this)) }
      >
          <Field
            name="firstName"
            component={ renderField }
            type="text"
            label="First Name"
          />
          <Field
            name="lastName"
            component={ renderField }
            type="text"
            label="Last Name"
          />

          <Field name="email" component={ renderField } type="email" label="Email" />

          <Field name="password" component={ renderField } type="password" label="Password" />

          <Field name="confirmPassword" component={ renderField } type="password" label="Confirm Password" />

        <Button type="primary">Submit</Button>
      </Form>
    );
  }
}

export default withRouter(reduxForm({
  form: 'userSignup',
  validate,
}, null)(UserSignupForm));
