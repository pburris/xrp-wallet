import * as actions from './actions';
import * as walletActions from '../Wallet/actions';


const authInitState = {
  authenticated: false,
  error: null,
  user: null,
};

const authReducer = (state = authInitState, action) => {
  switch(action.type) {
    case actions.AUTH_USER:
      return {
        ...state,
        authenticated: true,
      };
    case actions.UNAUTH_USER:
      return {
        ...state,
        authenticated: false,
      };
    case actions.AUTH_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case actions.SET_USER:
      return {
        ...state,
        user: action.user,
      };
    case actions.REMOVE_USER:
      return {
        ...state,
        user: null,
      };
    case walletActions.GENERATE_WALLET:
      return {
        ...state,
        user: {
          ...state.user,
          wallets: [
            ...state.user.wallets,
            action.wallet,
          ]
        }
      };
    default:
      return state;
  }
};

export default authReducer;
