import * as actions from './actions';

export const initState = {
  message: '',
};

export default function errorReducer(state = initState, action) {
  switch (action.type) {
    case actions.ADD_ERROR:
      return Object.assign({}, state, {
        message: action.message,
      });
    case actions.REMOVE_ERROR:
      return Object.assign({}, state, {
        message: '',
      });
    default:
      return state;
  }
}
