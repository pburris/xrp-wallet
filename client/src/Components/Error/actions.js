
export const ADD_ERROR = 'ADD_ERROR';
export const REMOVE_ERROR = 'REMOVE_ERROR';


export const addError = message => ({
  type: ADD_ERROR,
  message,
});


export const removeError = () => ({
  type: REMOVE_ERROR,
});
