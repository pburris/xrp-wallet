import React from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';


const Error = props => (
  <div>
    {
      props.message &&
      <div>
        { props.message }
        <span
          style={{ marginLeft: '30px', textDecoration: 'underline' }}
          onClick={ e => props.removeError() }
        >
          Click here to close
        </span>
      </div>
    }
  </div>
);


const mapStateToProps = state => ({
  message: state.error.message,
});

const mapDispatchToProps = dispatch => ({
  removeError: () => dispatch(actions.removeError()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Error);
