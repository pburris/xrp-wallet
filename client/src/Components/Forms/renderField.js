import React from 'react';
import { Form, Icon, Input } from 'antd';

const FormItem = Form.Item;

const renderField = ({
  input,
  label,
  type,
  className,
  meta: { touched, error }
}) => (
  <FormItem
    validationStatus={ error ? "error" : null }
    label={ label }
    help={ error ? error : null }
  >
    <Input
      { ...input }
      placeholder={ label }
      type={ type }
      id="error"
      hasFeedback
    />
  </FormItem>
);

export default renderField;
