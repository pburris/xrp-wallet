import React, { Component } from 'react'
import { connect } from 'react-redux';
import GetBalance from './GetBalance';

class PaymentTransaction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      destAddress: '',
      sendValue: '',
    }
  }

  paymentTx(e) {
    e.preventDefault();
    const tx = {
      sourceAddress: this.props.address,
      destAddress: this.state.destAddress,
      sendValue: this.state.sendValue,
      type: 'payment',
    };

    const opts = {
      method: 'POST',
      headers: {
        'authorization': `JWT ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(tx),
    };

    fetch('http://localhost:3001/wallet/transaction', opts)
      .then(res => res.json())
      .then(res => console.log(res));
  }

  render() {
    return (
      <div>
        <form>
          <div className="form--section">
          <label className="form--label">
            Sending Wallet:
            <input
              type="text"
              value={ this.props.address }
              disabled
              className="form--type__disabled"
            />
          </label>
          </div>

          <div className="form--section">
            <label className="form--label">
              Receiving wallet:
              <input
                type="text"
                value={ this.state.destAddress }
                onChange={ e => this.setState({ destAddress: e.target.value }) }
                className="form--type__text"
              />
            </label>
          </div>

          <div className="form--section">
            <label className="form--label">
              Amount
              <input
                type="text"
                value={ this.state.sendValue }
                onChange={ e => this.setState({ sendValue: e.target.value }) }
                className="form--type__text"
              />
            </label>
          </div>

          <button onClick={ this.paymentTx.bind(this) }>Send</button>
        </form>
        <GetBalance address={ this.props.address } />
      </div>
    );
  }
}

export default PaymentTransaction;
