import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from './actions';

class GenerateWallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wallets: [],
    };
    this.generate = this.generate.bind(this);
  }

  generate() {
    this.props.createWallet();
  }

  render() {
    return (
      <div>
        <button onClick={ e => {
          e.preventDefault();
          this.generate();
        }}>
          Generate Wallet
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  createWallet: () => dispatch(actions.createWallet())
});

export default connect(null, mapDispatchToProps)(GenerateWallet);
