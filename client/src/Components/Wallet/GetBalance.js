import React, { Component } from 'react';

export default class GetBalance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      balance: {},
    };
  }

  componentDidMount() {
    fetch(`http://localhost:3001/wallet/balance/${this.props.address}`)
      .then(res => res.json())
      .then(balance => this.setState({ balance }))
      .catch(console.log);
  }

  render() {
    return (
      <div>
        {
          this.state.balance.length &&
          <ul>
            {
              this.state.balance.map((entry, id) => (
                <li key={ id }>
                  <h3>
                    { entry.currency} - { entry.value }
                  </h3>
                </li>
              ))
            }
          </ul>
        }
      </div>
    );
  }
}
