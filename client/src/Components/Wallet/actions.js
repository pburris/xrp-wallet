import { actions as errorActions } from '../Error';


const ROOT_URL = 'http://localhost:3001';
const getJwtHeader = token => ({ 'authorization': `JWT ${token}` });


export const GENERATE_WALLET = 'GENERATE_WALLET';


export const generateWallet = wallet => ({
  type: GENERATE_WALLET,
  wallet,
});


export const createWallet = () =>
  dispatch =>
    fetch(`${ROOT_URL}/wallet/new`, { headers: getJwtHeader(localStorage.getItem('token')) })
      .then(res => res.json())
      .then(wallet => dispatch(generateWallet(wallet)))
      .catch(err => console.log(err))
