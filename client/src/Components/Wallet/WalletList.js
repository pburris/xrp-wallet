import React, { Component } from 'react'
import { connect } from 'react-redux';
import GetBalance from './GetBalance';
import PaymentTransaction from './PaymentTransaction';

class ShowWallets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sendModal: false,
      receiveModal: false,
      sendingWallet: '',
    };
  }

  activateSendModal(address) {
    return (function(e) {
      e.preventDefault();
      this.setState({
        sendModal: true,
        sendingWallet: address,
      });
    }).bind(this);
  }

  activateReceiveModal(e) {
    e.preventDefault();
    this.setState({
      receiveModal: true,
    });
  }

  render() {
    return (
      <div>
        <ul className="wallet--list">
          {
            this.props.wallets.length &&
            this.props.wallets.map(wallet => (
              <li key={ wallet.id }>
                <div className="wallet--address">{ wallet.address }</div>
                <div className="wallet--list__actions">
                  <button onClick={ this.activateSendModal(wallet.address) }>
                    Send
                  </button>
                  <button onClick={ this.activateReceiveModal }>
                    Receive
                  </button>
                </div>
              </li>
            ))
          }
        </ul>
          {
            this.state.sendModal &&
            <PaymentTransaction address={ this.state.sendingWallet } />
          }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  wallets: state.auth.user ? state.auth.user.wallets : [],
});

export default connect(mapStateToProps)(ShowWallets);
