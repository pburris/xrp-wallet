import React, { Component } from 'react';
import { Affix, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { withUserInfo } from '../User';


export class Header extends Component {
  render() {
    return (
      <Affix>
        <Menu mode="horizontal">
          <Menu.Item key="header-home">
            <h2><Link to="/">Ripple Wallet</Link></h2>
          </Menu.Item>
              {
                this.props.authenticated &&
                [
                  <Menu.Item key="header-wallets">
                    <Link to="/wallet">Wallets</Link>
                  </Menu.Item>,
                  <Menu.Item key="header-help">
                    <Link to="/help">Help</Link>
                  </Menu.Item>,
                  <Menu.Item key="header-signout">
                    <Link to="/signout">Sign Out</Link>
                  </Menu.Item>,
                ]
              }

              {
                !this.props.authenticated &&
                [
                  <Menu.Item key="header-signin">
                    <Link to="/signin">Sign In</Link>
                  </Menu.Item>,
                  <Menu.Item key="header-signup">
                    <Link to="/signup">Sign Up</Link>
                  </Menu.Item>,
                ]
              }
        </Menu>
      </Affix>
    );
  }
}

export default withUserInfo(Header);
