import React, { Component } from 'react';
import { Helmet } from 'react-helmet';


export default class Head extends Component {
  render() {
    return (
      <Helmet>
        <title>Ripple Wallet | Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Overpass+Mono:400,700" rel="stylesheet" />
      </Helmet>
    );
  }
}
