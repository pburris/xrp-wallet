import React, { Component } from 'react';
import { Signout } from '../../../Components/User';

class UserSignupPage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <Signout />
        </div>
      </main>
    );
  }
}

export default UserSignupPage;

