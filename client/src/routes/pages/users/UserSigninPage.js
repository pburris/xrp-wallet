import React, { Component } from 'react';
import { UserSigninForm } from '../../../Components/User';

class UserSigninPage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <h1>Signin</h1>
          <UserSigninForm />
        </div>
      </main>
    );
  }
}

export default UserSigninPage;
