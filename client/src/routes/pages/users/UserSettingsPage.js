import React, { Component } from 'react';
import { UserSettings } from '../../../Components/User';

class UserSettingsPage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <h1>Settings</h1>
          <UserSettings />
        </div>
      </main>
    );
  }
}

export default UserSettingsPage;
