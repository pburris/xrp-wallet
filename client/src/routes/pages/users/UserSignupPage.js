import React, { Component } from 'react';
import { UserSignupForm } from '../../../Components/User';

class UserSignupPage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <h1>Signup</h1>
          <UserSignupForm />
        </div>
      </main>
    );
  }
}

export default UserSignupPage;
