import React, { Component } from 'react';

class HomePage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <h1>Homepage</h1>
        </div>
      </main>
    );
  }
}

export default HomePage;
