import React, { Component } from 'react';
import { GenerateWallet, WalletList } from '../../../Components/Wallet';


class WalletIndexPage extends Component {
  render() {
    return (
      <main>
        <div className="container">
          <h1>Wallet</h1>
          <GenerateWallet />
          <WalletList />
        </div>
      </main>
    );
  }
}

export default WalletIndexPage;
