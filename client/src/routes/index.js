import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { withAuth } from '../Components/User';

// Pages
import HomePage from './pages/HomePage';

import UserSignupPage from './pages/users/UserSignupPage';
import UserSigninPage from './pages/users/UserSigninPage';
import UserSettingsPage from './pages/users/UserSettingsPage';
import UserSignoutPage from './pages/users/UserSignoutPage';

import WalletIndexPage from './pages/wallet/WalletIndexPage';


// Auth functions
const isAuth = withAuth(true);
const noAuth = withAuth(false);

export default class Routes extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Switch>
        <Route exact path="/" component={ HomePage } />
        <Route exact path="/help" component={ HomePage } />
        <Route exact path="/signup" component={ noAuth(UserSignupPage) } />
        <Route exact path="/signin" component={ noAuth(UserSigninPage) } />
        <Route exact path="/signout" component={ isAuth(UserSignoutPage) } />

        <Route exact path="/settings" component={ isAuth(UserSettingsPage) } />
        <Route exact path="/wallet" component={ isAuth(WalletIndexPage) } />

      </Switch>
    );
  }
}
