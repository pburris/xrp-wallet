import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { AUTH_USER, getUser } from './Components/User/actions';

import Client from './Client';
import store from './store';
import './styles/index.css';
//import registerServiceWorker from './registerServiceWorker';

// Check Auth
const token = localStorage.getItem('token');
if (token) {
  store.dispatch({ type: AUTH_USER });
  store.dispatch(getUser());
}

ReactDOM.render(
  (
    <Provider store={ store }>
      <BrowserRouter>
        <Client />
      </BrowserRouter>
    </Provider>
  ),
  document.getElementById('root')
);
//registerServiceWorker();
