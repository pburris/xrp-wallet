import React, { Component } from 'react';
import 'antd/dist/antd.css';
import Head from './Components/Head';
import Header from './Components/Header';
import { Error } from './Components/Error';
import Routes from './routes';


class Client extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Head />
        <Header />
        <Error />
        <Routes />
      </div>
    );
  }
}

export default Client;
