import { createStore, applyMiddleware, combineReducers } from 'redux';
import reduxThunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';

// Reducers
import { userReducer } from './Components/User';
import { errorReducer } from './Components/Error';


// Middleware
const middleware = [
  reduxThunk,
];

// Reducer
const rootReducer = combineReducers({
  form: formReducer,
  auth: userReducer,
  error: errorReducer,
});

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

export default createStoreWithMiddleware(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
